package com.ever.urbandictionary.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.os.Build

object NetworkUtils {
    fun hasNetwork(context: Context): Boolean? {

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val c : ConnectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork: Network? = c.activeNetwork
            val networkCapabilities = c.getNetworkCapabilities(activeNetwork)

            activeNetwork != null && networkCapabilities?.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED) == true
        } else {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
            activeNetwork != null && activeNetwork.isConnected && activeNetwork.isAvailable
        }
    }
}