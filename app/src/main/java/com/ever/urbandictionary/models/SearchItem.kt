package com.ever.urbandictionary.models

import androidx.room.Entity
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SearchItem (
    var list: List<Item>
)

@Entity(tableName = "item_table")
class Item(
    @SerializedName("definition")
    @Expose
    var definition: String,
    @SerializedName("thumbs_up")
    @Expose
    var thumbsUp: Int,
    @SerializedName("thumbs_down")
    @Expose
    var thumbsDown: Int,
    @Expose
    var word: String,
    @SerializedName("defid")
    @Expose
    var defid: Int,
    @SerializedName("example")
    @Expose
    var example: String
)