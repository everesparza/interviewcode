package com.ever.urbandictionary.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.ever.urbandictionary.models.Item

@Database(entities = [Item::class], version = 1)
abstract class UrbanDictionaryDatabase : RoomDatabase() {

    abstract fun itemDao(): ItemDao

    object DB {
        lateinit var instance: UrbanDictionaryDatabase

        @Synchronized
        fun getInstance(context: Context): UrbanDictionaryDatabase =
            Room.databaseBuilder(context.applicationContext,
                UrbanDictionaryDatabase::class.java, "item_database")
                .fallbackToDestructiveMigration()
                .addCallback(roomCallback)
                .build()

        private val roomCallback = object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
            }
        }
    }
}