package com.ever.urbandictionary.data.network

import com.ever.urbandictionary.models.SearchItem
import retrofit2.http.GET
import retrofit2.http.Query

interface UrbanDictionaryService {

    @GET("define")
    suspend fun getDefinitions(@Query("term") term: String)
            : SearchItem

}
