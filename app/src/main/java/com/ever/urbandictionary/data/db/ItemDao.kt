package com.ever.urbandictionary.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.ever.urbandictionary.models.Item

@Dao
interface ItemDao {

    @get:Query("SELECT * FROM item_table ORDER BY defid DESC")
    val allItems: LiveData<List<Item>>

    @Insert
    fun insert(item: Item)

    @Update
    fun update(item: Item)

    @Delete
    fun delete(item: Item)

    @Query("DELETE FROM item_table")
    fun deleteAllItems()
}
