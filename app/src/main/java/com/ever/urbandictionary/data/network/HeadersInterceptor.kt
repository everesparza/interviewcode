package com.ever.urbandictionary.data.network

import com.ever.urbandictionary.BuildConfig
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class HeadersInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original: Request = chain.request()

        val newRequest: Request = original
            .newBuilder()
            .header("X-RapidAPI-Key", BuildConfig.UDApiKey)
            .header("Accept", "application/json")
            .build()

        return chain.proceed(newRequest)
    }
}
