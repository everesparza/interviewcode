package com.ever.urbandictionary.data.network

import com.ever.urbandictionary.BuildConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object UrbanDictionaryRetrofit {
    fun get(): Retrofit {

        val httpClient =
            OkHttpClient
                .Builder()
                .addInterceptor(HeadersInterceptor())

        return Retrofit
            .Builder()
            .baseUrl(BuildConfig.UD_BASE_URL)
            .client(httpClient.build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}
