package com.ever.urbandictionary.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ever.urbandictionary.models.Item
import com.ever.urbandictionary.repositories.UrbanDicRepository
import com.ever.urbandictionary.repositories.UrbanDicRepositoryImpl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModelImpl(
    private val urbDicRepo: UrbanDicRepository = UrbanDicRepositoryImpl.getInstance()
) : MainViewModel, ViewModel() {

    private val _definitionList = MutableLiveData<List<Item>>()
    val definitionList: LiveData<List<Item>> = _definitionList

    private val _showProgress: MutableLiveData<Boolean> = MutableLiveData()
    val showProgress: LiveData<Boolean> = _showProgress

    override fun onSearchViewSubmitted(term: String) {
        _showProgress.value = true
        viewModelScope.launch(Dispatchers.IO) {
            _definitionList.postValue(urbDicRepo.getDefinitions(term))
            _showProgress.postValue(false)
        }
    }

    override fun sortList(sortBy: String) {
        viewModelScope.launch {
            val itemList = _definitionList.value
            when (sortBy) {
                "up" -> _definitionList.postValue(
                    itemList?.sortedBy {
                        it.thumbsUp })
                "down" -> _definitionList.postValue(
                    itemList?.sortedBy {
                        it.thumbsDown })
            }
        }
    }
}
