package com.ever.urbandictionary.repositories

import android.util.Log
import com.ever.urbandictionary.models.Item
import com.ever.urbandictionary.data.network.UrbanDictionaryRetrofit
import com.ever.urbandictionary.data.network.UrbanDictionaryService
import retrofit2.HttpException
import java.io.IOException
import java.lang.Exception


class UrbanDicRepositoryImpl private constructor() : UrbanDicRepository {
    private val TAG = UrbanDicRepositoryImpl::class.java.simpleName

    override suspend fun getDefinitions(term: String): List<Item> {
        var itemList = listOf<Item>()


        try {
            itemList = UrbanDictionaryRetrofit
                .get()
                .create(UrbanDictionaryService::class.java)
                .getDefinitions(term)
                .list

        } catch (httpEx: HttpException){
                Log.d(TAG, httpEx.message())
        } catch (ex: IOException) {
                Log.d(TAG, ex.message)
        } catch (genEx: Exception) {
                Log.d(TAG, genEx.message)
        }

        return itemList
    }

    companion object {
        @Volatile
        private var instance: UrbanDicRepositoryImpl? = null

        fun getInstance() = instance ?: synchronized(this) {
            UrbanDicRepositoryImpl().also { instance = it }
        }
    }
}
