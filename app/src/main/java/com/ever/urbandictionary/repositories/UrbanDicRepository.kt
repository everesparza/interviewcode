package com.ever.urbandictionary.repositories

import com.ever.urbandictionary.models.Item

interface UrbanDicRepository {
    suspend fun getDefinitions(term: String): List<Item>
}