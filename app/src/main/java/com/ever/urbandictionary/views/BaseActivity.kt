package com.ever.urbandictionary.views

import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity()
