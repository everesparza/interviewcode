package com.ever.urbandictionary.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.ever.urbandictionary.CoroutinesTestRule
import com.ever.urbandictionary.models.Item
import com.ever.urbandictionary.repositories.UrbanDicRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test


class MainViewModelImplTest {

    @ExperimentalCoroutinesApi
    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    lateinit var subject: MainViewModelImpl

    /**
     * Before the test runs initialize subject
     */
    @Before
    fun setup() {
        subject = MainViewModelImpl(FakeRepository())
        subject.onSearchViewSubmitted("term")
    }

    @Test
    fun sortListUpdatesList() {
        runBlocking {
            subject.sortList("up")

            subject.definitionList.observeForever {
                assert(it[0].defid.equals(1))
            }
        }
    }

    @Test
    fun getDefinitionList() {
        runBlocking {
            subject.onSearchViewSubmitted("term")
            subject.definitionList.observeForever {
                assert(it.isNotEmpty())
            }
        }
    }

}

class FakeRepository : UrbanDicRepository {
    override suspend fun getDefinitions(term: String): List<Item> {
        return listOf(
            Item("Definition 3", 3, 33, term, 3, "Example 3"),
            Item("Definition 2", 2, 22, term, 2, "Example 2"),
            Item("Definition 1", 1, 11, term, 1, "Example 1"))
    }
}
